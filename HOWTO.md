When updating files in either of these directories, use `add_sourceapp.sh` from the [Open GApps](https://github.com/opengapps/opengapps) project root.

* ***app***
* ***priv-app***
* ***lib***
* ***lib64***  

The other directories should be maintained manually.

---

**Nexus Images**

Some APKs can only be retrieved from nexus images.  

Download one of the latest Nexus Phone images from https://developers.google.com/android/nexus/image to extract the necessary applications from it.  

If the image of a phone is not used, some applications and libraries might be missing; e.g. the camera and/or the dialer.  

**NOTE:** Please make sure that APK files have the appropriate *classes.dex*!

1. Extract the image-xxxxx-xxxxxx.zip from the tgz archive  
2. Extract the `system.img` from the image-xxxxx-xxxxxx.zip  
3. Download the [sig2img](https://github.com/anestisb/android-simg2img) tool from github
4. Convert the `system.img` from a sparse android file to a disk-image file using `android-simg2img` as follows: 
```
simg2img system.img system.raw.img
```
5. Open the `system.raw.img` using a disk-mount application.

---

Import the following apks from the Nexus image using the `add_sourceapp.sh` script:  

```
/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk
/app/FaceLock/FaceLock.apk
/priv-app/GoogleBackupTransport/GoogleBackupTransport.apk
/priv-app/GoogleFeedback/GoogleFeedback.apk
/priv-app/GoogleLoginService/GoogleLoginService.apk
/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk
/priv-app/GooglePartnerSetup/GooglePartnerSetup.apk
/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk
/priv-app/SetupWizard/SetupWizard.apk
/priv-app/TagGoogle/TagGoogle.apk
/lib/libfacelock_jni.so
/lib/libfilterpack_facedetect.so
/lib/libjni_latinimegoogle.so
```

---

The following files and folders should be manually copied from the Nexus system image to their corresponding locations.

**Note that only the lib*.so files are architecture-dependent; the other files can go into 'all':**  

```/etc/permissions/com.google.android.camera2.xml
/etc/permissions/com.google.android.dialer.support.xml
/etc/permissions/com.google.android.maps.xml
/etc/permissions/com.google.android.media.effects.xml
/etc/permissions/com.google.widevine.software.drm.xml
/etc/preferred-apps/google.xml
/framework/com.google.android.camera2.jar
/framework/com.google.android.maps.jar
/framework/com.google.android.media.effects.jar
/framework/com.google.widevine.software.drm.jar
/usr/srec
/vendor/pittpatt
```
